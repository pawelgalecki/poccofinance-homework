package com.example.homework.converters;

import com.google.common.base.Preconditions;
import org.javamoney.moneta.Money;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.money.MonetaryContext;
import javax.money.MonetaryContextBuilder;
import java.math.BigDecimal;

@ConfigurationPropertiesBinding
@Component
public class MoneyConverter implements Converter<String, Money> {

    private final MonetaryContext context = MonetaryContextBuilder.of()
                            .setMaxScale(2)
                            .setFixedScale(true)
                            .build();


    @Override
    public Money convert(@NonNull String input) {
        String[] inputValue = input.trim().split("\\s+");
        String amount = inputValue[0];
        String currencyCode = inputValue[1];
        return Money.of(new BigDecimal(amount), currencyCode, context);
    }



}
