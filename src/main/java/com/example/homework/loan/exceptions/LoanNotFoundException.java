package com.example.homework.loan.exceptions;

import java.util.UUID;

@SuppressWarnings("UnusedReturnValue")
public class LoanNotFoundException extends RuntimeException {
    private final UUID missingLoanId;

    public LoanNotFoundException(UUID missingLoanId) {
        super("Loan with id:" + missingLoanId + " not found");
        this.missingLoanId = missingLoanId;
    }

    public UUID getMissingLoanId() {
        return missingLoanId;
    }
}