package com.example.homework.loan.exceptions;

import java.util.Set;

public class LoanApplicationValidationException extends RuntimeException {
    private final Set<String> errors;

    public LoanApplicationValidationException(Set<String> errors) {
        this.errors = errors;
    }


    public Set<String> getErrors() {
        return errors;
    }

}