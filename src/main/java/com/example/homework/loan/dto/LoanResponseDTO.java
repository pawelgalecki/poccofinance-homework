package com.example.homework.loan.dto;

import org.javamoney.moneta.Money;

import java.time.LocalDate;
import java.util.UUID;

@SuppressWarnings("UnusedReturnValue")
public class LoanResponseDTO {
    private final UUID loanId;
    private final Money issuedAmount;
    private final Money amountDue;
    private final LocalDate loanRepaymentDate;

    public LoanResponseDTO(UUID loanId, Money issuedAmount, Money amountDue, LocalDate loanRepaymentDate) {
        this.loanId = loanId;
        this.issuedAmount = issuedAmount;
        this.amountDue = amountDue;
        this.loanRepaymentDate = loanRepaymentDate;
    }

    public Money getIssuedAmount() {
        return issuedAmount;
    }

    public Money getAmountDue() {
        return amountDue;
    }

    public LocalDate getLoanRepaymentDate() {
        return loanRepaymentDate;
    }

    public UUID getLoanId() {
        return loanId;
    }
}
