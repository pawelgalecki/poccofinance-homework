package com.example.homework.loan.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

public class LoanApplicationDTO {
    private final Integer requestedTermInDays;
    private final MonetaryAmount requestedAmount;

    @JsonCreator
    public LoanApplicationDTO(@JsonProperty("requestedAmount") Money requestedAmount, @JsonProperty("requestedTermInDays") Integer requestedTermInDays) {
        this.requestedTermInDays = requestedTermInDays;
        this.requestedAmount = requestedAmount;
    }

    public Integer getRequestedTermInDays() {
        return requestedTermInDays;
    }

    public MonetaryAmount getRequestedAmount() {
        return requestedAmount;
    }


}
