package com.example.homework.loan;

import com.example.homework.loan.domain.LoanFacade;
import com.example.homework.loan.dto.LoanApplicationDTO;
import com.example.homework.loan.dto.LoanResponseDTO;
import com.example.homework.loan.exceptions.LoanApplicationValidationException;
import com.example.homework.loan.exceptions.LoanNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

@RestController
class LoanController {

    private final LoanFacade loanFacade;

    @Autowired
    public LoanController(LoanFacade loanFacade) {
        this.loanFacade = loanFacade;
    }


    @ExceptionHandler(LoanApplicationValidationException.class)
    @ResponseBody
    public HttpEntity invalidApplication(LoanApplicationValidationException exception) {
        Map<String, Set> errors = new HashMap<>();
        errors.put("errors", exception.getErrors());
        return ResponseEntity.badRequest().body(errors);
    }


    @ExceptionHandler(LoanNotFoundException.class)
    @ResponseBody
    public HttpEntity loanNotFound() {
        return ResponseEntity.notFound().build();
    }


    @GetMapping(value = "/loans/{loanId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity getLoan(@PathVariable UUID loanId) {
        LoanResponseDTO loan = loanFacade.getLoan(loanId);
        return ResponseEntity.ok(loan);
    }

    @PatchMapping(value = "/loans/{loanId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity extendLoan(@PathVariable UUID loanId) {
        LoanResponseDTO loan = loanFacade.extendLoan(loanId);
        return ResponseEntity.ok(loan);
    }


    @PostMapping(value = "/loans", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity applyForLoan(@RequestBody LoanApplicationDTO loanApplicationDTO, UriComponentsBuilder uriComponentsBuilder) {
        LoanResponseDTO loanResponseDTO = loanFacade.issueLoan(loanApplicationDTO);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/loans/{id}").buildAndExpand(loanResponseDTO.getLoanId());
        return ResponseEntity.created(uriComponents.toUri()).body(loanResponseDTO);
    }
}
