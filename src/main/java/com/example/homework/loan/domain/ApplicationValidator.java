package com.example.homework.loan.domain;


import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.Optional;

import static com.example.homework.loan.domain.ValidationError.*;

interface ApplicationValidator {
    Optional<ValidationError> validate(LoanApplication loanApplication);

    @Component
    class MaxAmountApplicationValidator implements ApplicationValidator {
        private final Money maximalAmount;

        public MaxAmountApplicationValidator(LoanServiceConfiguration loanServiceConfiguration) {
            this.maximalAmount = loanServiceConfiguration.getMaxAmount();
        }

        @Override
        public Optional<ValidationError> validate(LoanApplication loanApplication) {
            return loanApplication.getRequestedAmount().isGreaterThan(maximalAmount) ? Optional.of(AMOUNT_TOO_BIG) : Optional.empty();
        }
    }

    @Component
    class MaxTermApplicationValidator implements ApplicationValidator {

        private final Integer maximalTermInDays;

        public MaxTermApplicationValidator(LoanServiceConfiguration loanServiceConfiguration) {
            this.maximalTermInDays = loanServiceConfiguration.getMaxTermInDays();
        }

        @Override
        public Optional<ValidationError> validate(LoanApplication loanApplication) {
            return loanApplication.getTermInDays() > maximalTermInDays ? Optional.of(TERM_TOO_LONG) : Optional.empty();
        }
    }

    @Component
    class MinAmountApplicationValidator implements ApplicationValidator {
        private final Money minimalAmount;

        public MinAmountApplicationValidator(LoanServiceConfiguration loanServiceConfiguration) {
            this.minimalAmount = loanServiceConfiguration.getMinAmount();
        }

        @Override
        public Optional<ValidationError> validate(LoanApplication loanApplication) {
            return loanApplication.getRequestedAmount().isLessThan(minimalAmount) ? Optional.of(AMOUNT_TOO_SMALL) : Optional.empty();
        }
    }

    @Component
    class MinTermApplicationValidator implements ApplicationValidator {

        private final Integer minimalTermInDays;

        public MinTermApplicationValidator(LoanServiceConfiguration loanServiceConfiguration) {
            this.minimalTermInDays = loanServiceConfiguration.getMinTermInDays();
        }

        @Override
        public Optional<ValidationError> validate(LoanApplication loanApplication) {
            return loanApplication.getTermInDays()< minimalTermInDays ? Optional.of(TERM_TOO_SHORT) : Optional.empty();
        }
    }

    @Component
    class NightLoanApplicationValidator implements ApplicationValidator {

        private final Money maximalAmount;
        private final Clock clock;

        public NightLoanApplicationValidator(LoanServiceConfiguration loanServiceConfiguration, Clock clock) {
            this.maximalAmount = loanServiceConfiguration.getMaxAmount();
            this.clock = clock;
        }

        @Override
        public Optional<ValidationError> validate(LoanApplication loanApplication) {
            LocalTime localTime = LocalDateTime.ofInstant(clock.instant(), clock.getZone()).toLocalTime();
            if (localTime.getLong(ChronoField.MINUTE_OF_DAY) <= 360 && loanApplication.getRequestedAmount().equals(maximalAmount)) {
                return Optional.of(ValidationError.MAX_AMOUNT_AT_NIGHT);
            } else {
                return Optional.empty();
            }

        }
    }
}
