package com.example.homework.loan.domain;

import com.example.homework.loan.exceptions.LoanApplicationValidationException;
import org.javamoney.moneta.Money;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkState;

class CheckedLoanApplication {
    private final LoanApplication loanApplication;
    private final Set<ValidationError> validationErrors;

    CheckedLoanApplication(LoanApplication loanApplication, Set<ValidationError> validationErrors) {
        this.loanApplication = loanApplication;
        this.validationErrors = Collections.unmodifiableSet(validationErrors);
    }
    boolean canIssueLoan() {
        return validationErrors.isEmpty();
    }

    LoanApplicationValidationException toException() {
        checkState(!validationErrors.isEmpty());

        return new LoanApplicationValidationException(this.validationErrors
                .stream()
                .map(ValidationError::getI18nMessageKey)
                .collect(Collectors.toSet()));
    }

    Money getRequestedAmount() {
        return loanApplication.getRequestedAmount();
    }

    Integer getTermInDays() {
        return loanApplication.getTermInDays();
    }
}
