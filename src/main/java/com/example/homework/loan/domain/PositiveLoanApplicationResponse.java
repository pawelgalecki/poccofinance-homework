package com.example.homework.loan.domain;

import com.example.homework.loan.dto.LoanResponseDTO;
import org.javamoney.moneta.Money;

import java.time.LocalDate;
import java.util.UUID;

class PositiveLoanApplicationResponse {
    private final UUID loanId;
    private final Money issuedAmount;
    private final Money amountDue;
    private final LocalDate loanRepaymentDate;

    PositiveLoanApplicationResponse(UUID loanId, Money issuedAmount, Money amountDue, LocalDate loanRepaymentDate) {
        this.loanId = loanId;
        this.issuedAmount = issuedAmount;
        this.amountDue = amountDue;
        this.loanRepaymentDate = loanRepaymentDate;
    }

    LoanResponseDTO toDTO() {
        return new LoanResponseDTO(loanId, issuedAmount, amountDue, loanRepaymentDate);
    }
}