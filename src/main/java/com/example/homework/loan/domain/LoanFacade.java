package com.example.homework.loan.domain;

import com.example.homework.loan.dto.LoanApplicationDTO;
import com.example.homework.loan.dto.LoanResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LoanFacade {
    private final List<ApplicationValidator> validators;
    private final LoanService loanService;

    public LoanFacade(List<ApplicationValidator> validators, LoanService loanService) {
        this.validators = validators;
        this.loanService = loanService;
    }


    private CheckedLoanApplication checkApplication(LoanApplication loanApplication) {
        Set<ValidationError> validationErrors = validators
            .stream()
            .map(applicationValidator -> applicationValidator.validate(loanApplication))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toSet());
        return new CheckedLoanApplication(loanApplication, validationErrors);
    }

    public LoanResponseDTO issueLoan(LoanApplicationDTO loanApplicationDTO) {
        LoanApplication loanApplication = LoanApplication.fromDTO(loanApplicationDTO);
        CheckedLoanApplication checkedLoanApplication = checkApplication(loanApplication);

        if (checkedLoanApplication.canIssueLoan()) {
            return loanService.issueLoan(checkedLoanApplication).toDTO();
        } else {
            throw checkedLoanApplication.toException();
        }
    }

    public LoanResponseDTO getLoan(UUID loanId) {
        return loanService.getLoan(loanId).toDTO();
    }

    public LoanResponseDTO extendLoan(UUID loanId) {
        return loanService.extendLoan(loanId).toDTO();
    }
}
