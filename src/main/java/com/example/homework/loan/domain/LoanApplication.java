package com.example.homework.loan.domain;

import com.example.homework.loan.dto.LoanApplicationDTO;
import org.javamoney.moneta.Money;

class LoanApplication {
    private final Money requestedAmount;
    private final Integer requestedLoanPeriodInDays;

    private LoanApplication(Money requestedAmount, Integer requestedLoanPeriodInDays) {
        this.requestedAmount = requestedAmount;
        this.requestedLoanPeriodInDays = requestedLoanPeriodInDays;
    }

    Money getRequestedAmount() {
        return requestedAmount;
    }

    Integer getTermInDays() {
        return this.requestedLoanPeriodInDays;
    }

    static LoanApplication fromDTO(LoanApplicationDTO dto) {
        return new LoanApplication(Money.from(dto.getRequestedAmount()), dto.getRequestedTermInDays());
    }

}