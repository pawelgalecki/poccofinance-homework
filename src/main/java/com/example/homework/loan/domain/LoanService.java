package com.example.homework.loan.domain;

import com.example.homework.loan.exceptions.LoanNotFoundException;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDate;
import java.util.UUID;

@Service
class LoanService {
    private final Clock clock;
    private final LoanRepository loanRepository;
    private final LoanServiceConfiguration configuration;

    public LoanService(Clock clock, InMemoryLoanRepository loanRepository, LoanServiceConfiguration configuration) {
        this.clock = clock;
        this.loanRepository = loanRepository;
        this.configuration = configuration;
    }

    PositiveLoanApplicationResponse issueLoan(CheckedLoanApplication loanApplication) {
        Money requestedAmount = loanApplication.getRequestedAmount();
        Money amountDue = loanApplication.getRequestedAmount().multiply(1.1);
        LocalDate loanRepaymentDate = clock.instant().atZone(clock.getZone()).plusDays(loanApplication.getTermInDays()).toLocalDate();
        Loan loan = new Loan(requestedAmount, amountDue, loanRepaymentDate);
        loanRepository.save(loan);
        return loan.asLoanApplicationResponse();
    }

    PositiveLoanApplicationResponse getLoan(UUID loanId) {
        return loanRepository.find(loanId).map(Loan::asLoanApplicationResponse).orElseThrow(()->new LoanNotFoundException(loanId));
    }

    PositiveLoanApplicationResponse extendLoan(UUID loanId) {
        return loanRepository.find(loanId)
            .map(loan -> loan.extend(configuration.getExtensionTermInDays()))
            .map(Loan::asLoanApplicationResponse)
            .orElseThrow(()->new LoanNotFoundException(loanId));
    }
}
