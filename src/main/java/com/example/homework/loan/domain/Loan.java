package com.example.homework.loan.domain;

import org.javamoney.moneta.Money;

import java.time.LocalDate;
import java.util.UUID;

class Loan {
    private final UUID id;
    private final Money issuedAmount;
    private final Money amountDue;
    private LocalDate loanRepaymentDate;
    private Integer numberOfExtensions;


    Loan(Money issuedAmount, Money amountDue, LocalDate loanRepaymentDate) {
        this.id=UUID.randomUUID();
        this.issuedAmount = issuedAmount;
        this.amountDue = amountDue;
        this.loanRepaymentDate = loanRepaymentDate;
        this.numberOfExtensions=0;
    }

    Loan extend(Integer numberOfDays) {
        numberOfExtensions++;
        loanRepaymentDate=loanRepaymentDate.plusDays(numberOfDays);
        return this;
    }

    PositiveLoanApplicationResponse asLoanApplicationResponse() {
        return new PositiveLoanApplicationResponse(id, issuedAmount, amountDue, loanRepaymentDate);
    }

    UUID getId() {
        return id;
    }
}
