package com.example.homework.loan.domain;

import java.util.Optional;
import java.util.UUID;

interface LoanRepository {
    void save(Loan loan);

    Optional<Loan> find(UUID loanId);
}
