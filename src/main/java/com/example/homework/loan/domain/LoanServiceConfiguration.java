package com.example.homework.loan.domain;

import org.javamoney.moneta.Money;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="loan")
@Component

class LoanServiceConfiguration {
    private Money minAmount;
    private Money maxAmount;
    private Integer minTermInDays;
    private Integer maxTermInDays;
    private Integer extensionTermInDays;

    public Money getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Money minAmount) {
        this.minAmount = minAmount;
    }

    public Money getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Money maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMinTermInDays() {
        return minTermInDays;
    }

    public void setMinTermInDays(Integer minTermInDays) {
        this.minTermInDays = minTermInDays;
    }

    public Integer getMaxTermInDays() {
        return maxTermInDays;
    }

    public void setMaxTermInDays(Integer maxTermInDays) {
        this.maxTermInDays = maxTermInDays;
    }

    public Integer getExtensionTermInDays() {
        return extensionTermInDays;
    }

    public void setExtensionTermInDays(Integer extensionTermInDays) {
        this.extensionTermInDays = extensionTermInDays;
    }
}