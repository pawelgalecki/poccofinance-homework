package com.example.homework.loan.domain;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository
class InMemoryLoanRepository implements LoanRepository {
    private final Map<UUID, Loan> loans = new HashMap<>();

    @Override
    public void save(Loan loan) {
        loans.put(loan.getId(), loan);
    }

    @Override
    public Optional<Loan> find(UUID loanId) {
        return Optional.ofNullable(loans.get(loanId));
    }
}
