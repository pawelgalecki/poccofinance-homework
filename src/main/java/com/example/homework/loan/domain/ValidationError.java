package com.example.homework.loan.domain;

enum ValidationError {

    AMOUNT_TOO_SMALL("amount.too.small"),
    AMOUNT_TOO_BIG("amount.too.big"),
    TERM_TOO_LONG("term.too.long"),
    TERM_TOO_SHORT("term.too.short"),
    MAX_AMOUNT_AT_NIGHT("max.amount.at.night");

    private final String i18nMessageKey;

    public String getI18nMessageKey() {
        return i18nMessageKey;
    }

    ValidationError(String description) {
        this.i18nMessageKey = description;
    }
}
