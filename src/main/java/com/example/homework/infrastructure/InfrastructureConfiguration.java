package com.example.homework.infrastructure;

import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.jackson.datatype.money.MoneyModule;

import java.time.Clock;
import java.util.TimeZone;

@Configuration
class InfrastructureConfiguration {
    @Bean
    Clock getClock() {
        return Clock.system((TimeZone.getTimeZone("Warsaw/Europe").toZoneId()));
    }

    @Bean
    Jackson2ObjectMapperBuilderCustomizer customizeJackson() {
        return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder
            .modulesToInstall(new MoneyModule())
            .featuresToEnable(SerializationFeature.INDENT_OUTPUT);
    }
}


