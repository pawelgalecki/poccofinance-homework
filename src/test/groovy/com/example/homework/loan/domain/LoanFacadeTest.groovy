package com.example.homework.loan.domain

import com.example.homework.loan.dto.LoanApplicationDTO
import com.example.homework.loan.dto.LoanResponseDTO
import com.example.homework.loan.exceptions.LoanApplicationValidationException
import com.example.homework.loan.exceptions.LoanNotFoundException
import org.javamoney.moneta.Money
import org.springframework.context.support.StaticApplicationContext
import org.threeten.extra.MutableClock
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import javax.money.MonetaryContextBuilder
import java.time.Clock
import java.time.Instant
import java.time.LocalDate

class LoanFacadeTest extends Specification {

    @Subject
    LoanFacade loanFacade

    @Shared
    def context

    @Shared
    LoanRepository repository


    def setupSpec() {
        context = new StaticApplicationContext()
        context.registerBean(LoanServiceConfiguration.class, {->
            new LoanServiceConfiguration(
                    minAmount: Money.of(10, "PLN"),
                    maxAmount: Money.of(10_000, "PLN"),
                    maxTermInDays: 90,
                    minTermInDays: 10,
                    extensionTermInDays: 10
            )
        })
        context.registerBean(Clock.class, {->
            MutableClock.of(Instant.now(), TimeZone.getTimeZone("Warsaw/Europe").toZoneId())
        })

        context.registerBean(ApplicationValidator.MaxAmountApplicationValidator.class)
        context.registerBean(ApplicationValidator.MinAmountApplicationValidator.class)
        context.registerBean(ApplicationValidator.MaxTermApplicationValidator.class)
        context.registerBean(ApplicationValidator.MinTermApplicationValidator.class)
        context.registerBean(ApplicationValidator.NightLoanApplicationValidator.class)
        context.registerBean(InMemoryLoanRepository.class)
        context.registerBean(LoanService.class)
        context.registerBean(LoanFacade.class)
    }

    def setup() {
        loanFacade = context.getBean(LoanFacade.class)
        repository = context.getBean(LoanRepository.class)
    }

    def "should extend loan"() {
        given: 'a loan has been issued'
            Money issued = Money.of(10_000, "PLN")
            Money due = Money.of(11_000, "PLN")
            Loan loan = new Loan(issued, due, LocalDate.of(2019, 01, 20))
            repository.save(loan)
        when: 'one extends this loan'
            LoanResponseDTO response = loanFacade.extendLoan(loan.getId())
        then: 'this loan is extended by "extensionTermInDays"'
            with(response) {
                response.amountDue == due
                response.issuedAmount == issued
                response.loanId == loan.id
                response.loanRepaymentDate == LocalDate.of(2019, 01, 30)
            }
    }

    def "should not extend loan if this loan has never been issued"() {
        given: 'there is a loan that '
            Money issue = Money.of(10_000, "PLN")
            Money due = Money.of(11_000, "PLN")
            Loan loan = new Loan(issue, due, LocalDate.of(2019, 01, 20))
        when:
            loanFacade.extendLoan(loan.id)
        then:
            LoanNotFoundException e = thrown(LoanNotFoundException)
            e.missingLoanId==loan.id
    }

    @Unroll
    def "should issue requested loan if the application is valid or a set of errors otherwise"() {
        given:
            def clock = context.getBean(Clock.class)
            Money requestedAmount = Money.of(applicationAmount, "PLN", MonetaryContextBuilder.of().setMaxScale(2).build())
            LoanApplicationDTO application = new LoanApplicationDTO(requestedAmount, requestedLoanPeriodInDays)
        when:
            clock.setInstant(Instant.parse("${date}T${time}:00Z"))
            def response
            try {
                response = loanFacade.issueLoan(application)
            } catch (Exception e) {
                response = e
            }

            def result = interpretResponse(response)
        then:
            expectedResult == result

        where:
            date         | time    | applicationAmount | requestedLoanPeriodInDays || expectedResult
            "2011-11-01" | "07:00" | 10_000            | 30                        || "amount due: PLN 11000.00/repayment date: 2011-12-01"
            "2011-11-01" | "08:12" | 10_000            | 31                        || "amount due: PLN 11000.00/repayment date: 2011-12-02"
            "2091-01-21" | "09:12" | 10_000            | 10                        || "amount due: PLN 11000.00/repayment date: 2091-01-31"
            "2018-12-31" | "01:12" | 11.11             | 89                        || "amount due: PLN 12.22/repayment date: 2019-03-30"
            "2011-11-01" | "00:59" | 10_000            | 30                        || ["max.amount.at.night"] as Set
            "2011-11-01" | "02:22" | 10_000            | 30                        || ["max.amount.at.night"] as Set
            "2011-11-01" | "00:00" | 10_000            | 30                        || ["max.amount.at.night"] as Set
            "2011-11-01" | "05:59" | 10_000            | 30                        || ["max.amount.at.night"] as Set
            "2011-11-01" | "06:00" | 10_000            | 30                        || ["max.amount.at.night"] as Set
            "2011-11-01" | "06:00" | 10_000            | 300                       || ["max.amount.at.night", "term.too.long"] as Set
            "2011-11-01" | "01:12" | 9                 | 30                        || ["amount.too.small"] as Set
            "2011-11-01" | "08:12" | 9.99              | 30                        || ["amount.too.small"] as Set
            "2011-11-01" | "06:12" | 0                 | 30                        || ["amount.too.small"] as Set
            "2011-11-01" | "06:12" | -1                | 30                        || ["amount.too.small"] as Set
            "2011-11-01" | "06:12" | 10_001            | 30                        || ["amount.too.big"] as Set
            "2011-11-01" | "06:12" | 10_001            | 3                         || ["term.too.short", "amount.too.big"] as Set
            "2011-11-01" | "06:12" | 1                 | 300                       || ["term.too.long", "amount.too.small"] as Set
            "2011-11-01" | "01:12" | 20_000            | 300                       || ["term.too.long", "amount.too.big"] as Set

    }

    def interpretResponse(response) {
        def result
        switch (response) {
            case LoanResponseDTO:
                response = response as LoanResponseDTO
                result = "amount due: ${response.getAmountDue()}/repayment date: ${response.getLoanRepaymentDate()}"
                break
            case LoanApplicationValidationException:
                response = response as LoanApplicationValidationException
                result = response.errors
                break
            default:
                assert false, "failed"
                break
        }
        return result
    }


}


