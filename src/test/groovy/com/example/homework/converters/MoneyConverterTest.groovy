package com.example.homework.converters


import org.javamoney.moneta.Money
import spock.lang.Specification
import spock.lang.Unroll

class MoneyConverterTest extends Specification {
    MoneyConverter converter = new MoneyConverter()

    @Unroll
    def "should correctly convert from String to Money"() {
        given:
        when:
            Money result = converter.convert(input)
        then:
            result.currency.toString() == 'PLN'
            result.getNumber().toString() == expectedValue
        where:
            input            || expectedValue
            "0.9 PLN"        || "0.90"
            ".9  PLN"        || "0.90"
            "909 PLN"        || "909.00"
            "12.12 PLN"      || "12.12"
            "12.121 PLN"     || "12.12"
            "0.99 PLN"       || "0.99"
            "0.98999999 PLN" || "0.99"

    }
}
