package com.example.homework

import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.LocalHostUriTemplateHandler
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.core.env.Environment
import org.springframework.http.HttpMethod
import org.springframework.http.HttpRequest
import org.springframework.http.ResponseEntity
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.test.context.TestPropertySource
import org.springframework.util.MimeTypeUtils
import org.threeten.extra.MutableClock
import spock.lang.Specification

import java.time.Clock
import java.time.Instant
import java.time.ZoneId
import java.util.regex.Matcher

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
class LoanApplicationTest extends Specification {

    @Autowired
    TestRestTemplate testRestTemplate

    @Autowired
    Environment environment

    @Autowired
    Clock clock


    def setup() {
        testRestTemplate = new TestRestTemplate()
        testRestTemplate.setUriTemplateHandler(new LocalHostUriTemplateHandler(environment))
        testRestTemplate.restTemplate.interceptors = [contentTypeInterceptor()]

        //workaround for https://jira.spring.io/browse/SPR-15052
        CloseableHttpClient httpClient = HttpClientBuilder.create().build()
        testRestTemplate.restTemplate.requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient)

    }


    def 'Should grant loan and extend a loan'() {
        given: 'I have a loan application'
            String loanApplication = """
                | {
                |   "requestedTermInDays": 40,
                |   "requestedAmount": {
                |      "amount":1000,
                |      "currency":"PLN"
                |   }
                | }
            """.stripMargin()
        and: 'today is 7:00 on 1st of November 2011'
            clock.set(Instant.parse("2011-11-01T07:00:00Z"))

        when: 'I apply for a loan with this application'
            ResponseEntity<String> loanApplicationResponse = testRestTemplate.postForEntity("/loans", loanApplication, String.class)

        then: 'I am granted a loan'
              loanApplicationResponse.statusCode.value() == 201
              loanApplicationResponse.headers['Location'].size()==1
              String location = loanApplicationResponse.headers['Location'][0]
              String id = extractLoanIdFromLocation(location)


            loanApplicationResponse.body == """
                |{
                |  "loanId" : "$id",
                |  "issuedAmount" : {
                |    "amount" : 1000.00,
                |    "currency" : "PLN"
                |  },
                |  "amountDue" : {
                |    "amount" : 1100.00,
                |    "currency" : "PLN"
                |  },
                |  "loanRepaymentDate" : "2011-12-11"
                |}
                """.stripMargin().trim()

        when: 'I extend the granted loan'
            ResponseEntity<String> loanCheck = testRestTemplate.exchange(location, HttpMethod.PATCH, null, String.class)
            testRestTemplate.patchForObject(location,null, String.class)
        then: 'I have 10 more days to pay it back'
        then:
            loanCheck.statusCode.value() == 200
            loanCheck.body == """
                |{
                |  "loanId" : "$id",
                |  "issuedAmount" : {
                |    "amount" : 1000.00,
                |    "currency" : "PLN"
                |  },
                |  "amountDue" : {
                |    "amount" : 1100.00,
                |    "currency" : "PLN"
                |  },
                |  "loanRepaymentDate" : "2011-12-21"
                |}
                """.stripMargin().trim()
    }

    private static String extractLoanIdFromLocation(String location) {
        Matcher matcher = location =~ /.*\\/loans\\/(.*)/
        matcher.find()
        String id = matcher.group(1)
        return id
    }

    private static ClientHttpRequestInterceptor contentTypeInterceptor() {
        new ClientHttpRequestInterceptor() {
            @Override
            ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
                request.headers.set("Accept", MimeTypeUtils.APPLICATION_JSON_VALUE)
                request.headers.set("Content-Type", MimeTypeUtils.APPLICATION_JSON_VALUE)
                return execution.execute(request, body)
            }
        }
    }

    @TestConfiguration
    static class ClockConfiguration {
        @Bean
        @Primary
        Clock clock() {
            ZoneId id = TimeZone.getTimeZone("Warsaw/Europe").toZoneId()
            return MutableClock.of(Instant.now(), id)
        }
    }
}
